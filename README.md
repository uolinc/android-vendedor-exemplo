![ps5.png](https://bitbucket.org/repo/4naLKz/images/1051242651-ps5.png)

# Guia de Integração #
* **
**Biblioteca Android Manual de Uso**

* **
   **Histórico de Versões**                                                                                      
   - 0.1 : **Visão Geral**  - 05/11/2014      
   - 0.1.6:  **Implementação base para uso e configurações** - 26/02/2015         
   - 0.2.0: **Implementação com serviços assincronos para facilitar o uso da lib** - 24/03/2015
* **
## **Copyright** ##
Todos os direitos reservados. O UOL é uma marca comercial do UNIVERSO ONLINE S / A. O logotipo do UOL é uma marca comercial do UNIVERSO ONLINE S / A. Outras marcas, nomes, logotipos e marcas são de propriedade de seus respectivos proprietários.
As informações contidas neste documento pertencem ao UNIVERSO ONLINE S/A. Todos os direitos reservados. UNIVERSO ONLINE S/A. - Av. Faria Lima, 1384, 6º andar, São Paulo / SP, CEP 01452-002, Brasil.
O serviço PagSeguro não é, nem pretende ser comparável a serviços financeiros oferecidos por instituições financeiras ou administradoras de cartões de crédito, consistindo apenas de uma forma de facilitar e monitorar a execução das transações de comércio electrónico através da gestão de pagamentos. Qualquer transação efetuada através do PagSeguro está sujeita e deve estar em conformidade com as leis da República Federativa do Brasil.
Aconselhamos que você leia os termos e condições cuidadosamente.


## **Aviso Legal** ##
O UOL não oferece garantias de qualquer tipo (expressas, implícitas ou estatutárias) com relação às informações nele contidas. O UOL não assume nenhuma responsabilidade por perdas e danos (diretos ou indiretos), causados por erros ou omissões, ou resultantes da utilização deste documento ou a informação contida neste documento ou resultantes da aplicação ou uso do produto ou serviço aqui descrito. O UOL reserva o direito de fazer qualquer tipo de alterações a quaisquer informações aqui contidas sem aviso prévio.

* **

## **Visão Geral** ##
A biblioteca PagSeguro Vendedor tem como foco auxiliar desenvolvedores que desejam fornecer em seus aplicativos toda a praticidade e segurança fornecida pelo PagSeguro no segmento de pagamentos móveis através de smartphones e tablets. Para ajudar a entender como a aplicação da biblioteca deve ser realizada e as possibilidades disponíveis, apresentamos os seguintes cenários:

• Cenário 1: Solução de pagamentos C2C
A empresa X desenvolve um aplicativo para seus clientes permitindo-os efetuar a cobrança de serviços prestados ou itens vendidos aos seus próprios clientes. Neste cenário o cliente da empresa X autoriza a aplicação a realizar vendas em seu nome utilizando sua conta PagSeguro. Além disso, a empresa X PODE configurar uma porcentagem ou taxa de comissão sobre a venda efetuada por seu cliente.

• Cenário 2: Solução de pagamento C2B
A empresa X desenvolve um aplicativo para uso de seus funcionários. A conta da empresa X no PagSeguro é utilizada para efetuar as transações permitindo, desta forma, que a empresa receba os valores de vendas efetuadas por seus funcionários.

* **
## **Conceitos Básicos** ##
Antes de fazer uso da biblioteca é importante que o desenvolvedor realize alguns procedimentos básico de cadastro e preparação, além de assimilar alguns conceitos importantes para o correto funcionamento de sua aplicação. 

* **

## **Cadastro da aplicação** ##
O primeiro passo para o desenvolvimento da aplicação é realizar o cadastro da aplicação PagSeguro no site.
A documentação referente a aplicação pode ser acessada em: 
http://download.uol.com.br/pagseguro/docs/pagseguro-modelo-aplicacoes.pdf

* **
## **Conta de usuário PagSeguro** ##
Para realizar transações utilizando a biblioteca é necessária uma conta PagSeguro. Esta conta pode ser da empresa desenvolvedora do aplicativo ou de seu cliente, que irá fazer uso da aplicação.
Através da biblioteca é possível solicitar à um usuário PagSeguro a autorização de uso de sua conta pela biblioteca num processo bastante semelhante ao que alguns jogos e aplicativos fazem com o Facebook. Mais uma vez, a utilização desta funcionalidade pode variar de acordo com o uso dado ao aplicativo. 
Após a autorização por parte do usuário são fornecidas credenciais de uso da biblioteca. Estas credenciais devem ser salvas pelo aplicativo pois serão requisitadas em cada chamada feita à biblioteca.

* **

## **Leitor de cartões PagSeguro** ##
Para realizar transações utilizando a biblioteca também é necessário um leitor de cartões PagSeguro. Este leitor utiliza a tecnologia Bluetooth para se comunicar com celulares e tablets. Por este motivo, é importante informar aos usuários de sua aplicação sobre a necessidade de se utilizar um aparelho que disponha desta funcionalidade.
O processo de instalação do leitor de cartões envolve duas etapas, ambos realizados através de chamadas para a biblioteca, são elas: Pareamento, responsável por fazer o link Bluetooth entre o celular/tablet e o leitor e Configuração, onde são realizadas comunicações com o leitor e o servidor PagSeguro para habilitar o uso do leitor dentro de sua aplicação. Por este motivo, é importante sinalizar ao usuário a necessidade de uma conexão à internet ativa durante este processo.

Caso você ainda não tenha um leitor do PagSeguro [clique aqui](https://pagseguro.uol.com.br/venda-pelo-celular/leitor-de-debito-e-credito.html).

* **
## **Instalação** ##
Via gradle:

```
#!json
dependencies {
    ...
    compile 'br.com.uol.pslibs:pagseguro-lib:0.2.0'
    ...
}

```


* **
## **Metodologia** ##
Vejamos agora como integrar a biblioteca do PagSeguro em seu aplicativo Android.

Para utilizar a library do PagSeguro UOL para Android são nescessários 5 etapas:

**1 – Implementação base:** Inicialização da Library e controles  definidos durante o ciclo de vida da Activity;


**
2 – Autorização:** Configurações de credenciais da conta PagSeguro para realização das transações;


**
3 – Pareamento e instalação:** Para que seja possível utlizar os leitores bluetooth é nescessário o pareamento e instalação, verifique o tópico sobre pareamento e instalação nas paginas seguintes deste documento;


**
4 – Pagamentos:** Os pagamentos serão realizados de maneiras simples e facil após os passos acima estiverem ok, basta verificar a utilização dos metodos da library e a parametrização nescessária;

**5 – Estorno:**  Para um pefeito funcionamento de uma metodologia de pagamento é importante o estorno estar funcionando, basta verificar os metodos utlizados da library,   consulte o funcionamento e exemplos contido  nesse documento.
* **


## **Implementação base** ##
Antes de realizar as chamadas para realizar pagamento, temos de inicializar a biblioteca. Este processo deve ser realizado uma única vez durante o ciclo de vida da aplicação. Além disso, existem alguns métodos que devem ser chamados e eventos específicos de uma Activity, garantindo que a biblioteca possa controlar e liberar todos os recursos utilizados assim que necessário. Assumindo que sua aplicação utiliza uma Activity com diversos Fragments no processo de venda, você deve efetuar este procedimento conforme o código abaixo:



```
#!java

public class MainActivity extends Activity
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Configuration.Builder builder = new Configuration.Builder(this);
		//Metodo de inicializaçao da lib
		PSLibrary.initialize(builder.create())
	}
	@Override
	protected void onStart() {
		super.onStart();
		PSLibrary.onStart(this); //Controle Lib Activity Life Cycle
	}
	@Override
	protected void onStop() {
		super.onStop();
		PSLibrary.onStop(); //Controle Lib Activity Life Cycle
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		PSLibrary.dispose(); //Controle Lib Activity Life Cycle
	}
}

```
Ao utilizar o código acima você habilita o uso da biblioteca enquanto a Activity em que o código reside esteja ativa bastando apenas efetuar as chamadas para as rotinas desejadas, é de extrema importância para o funcionamento da “Library” as chamadas dos metodos do ciclo de vida da activity apontado no codigo mostrado acima.

* **
## **Sincrono vs Assincrono** ##
A API de operação do leitor do PagSeguro necessita fazer operações de IO e também requisições via rede internet, o que obriga a execução em uma Thread diferente da UIThread. 


```
#!java
// Este é um exemplo de uma chamada síncrona, que retorna o resultado
// na resposta da função e que não retorna nada enquanto todo o fluxo do serviço 
// não seja retornado, bloqueando assim a Thread atual.
// Para executar corretamente deve estar sendo executado em outra Thread, ou como caso
// mais simples, utilizando uma AsyncTask.
new AsyncTask<Void, Void, Objeto>() {
    public Objeto runOnBackground() {
        Exception failure;
        try {
            return PSLibrary.method(param, param2);
        } catch (Exception ex) {
            failure = ex;
            return null;
        }
    }
    public void onPostExecute(Objeto result) {
        if (failure != null || result == null) {
            // TRATAR ERRO DA OPERAÇÃO
        } else {
            // TRATAR SUCESSO DA OPERAÇÃO
        }
    }
}.execute();


// O mesmo serviço agora pode ser chamado de forma assincrona, facilitando e diminuindo o boilerplate
// de cada chamada. Veja abaixo a mesma chamada acima, porém de forma Assíncrona.
PSLibrary.method(param, param2, new PSCallback<Objeto>() {
    public void success(Objeto result) {
        // TRATAR SUCESSO DA OPERAÇÃO
    }
    public void failure(Throwable ex) {
        // TRATAR ERRO DA OPERAÇÃO
    }
});

```

Definida a estratégia a ser utilizada (Sincrona ou Assincrona), todos os serviços possuem 2 versões: Síncrona e Assíncrona.


* **
## **Autorização** ##
Conforme descrito anteriormente, para fazer uso das rotinas da biblioteca é necessário obter a autorização do usuário de uma conta PagSeguro para que as vendas possam ser realizadas em seu nome e o dinheiro pago vá para a conta do usuário. Este procedimento é feito através uma rotina da biblioteca que permite fornecer os dados da aplicação solicitante e recuperar, em caso de aceite do usuário, as credenciais para uso. Essas credenciais devem ser armazenadas pela aplicação para uso posterior e, em caso de expiração, serem atualizadas também através da biblioteca. Para solicitar a autorização do usuário utilize o código abaixo:


```
#!java
// Neste objeto colocamos os dados da aplicação solicitante
// Estes dados podem ser obtidos durante o cadastro da aplicação no site do 
// PagSeguro. Para maiores informações consulte a classe PSApp no Apêndice A
// deste documento
PSApp app = new PSApp(“appId”,”appKey”, new String[] { “perm1”, “perm2” });

// Passamos os dados da aplicação solicitante e a Activity onde o usuário está
// para que possa ser exibida a caixa de diálogo com a solicitação de autoriza
// ção
// SINCRONA (bloqueia  a thread e não pode ser chamado a partir da UIThread)
OAuthCredentials cred = PSLibrary.authorize(app, MainActivity.this);

// * OU *

// ASSINCRONA
PSLibrary.authorize(app, MainActivity.this, new PSCallback<OAuthCredentials>(){ /*...*/ });
```
Feito isso a aplicação recebe as credenciais e deve armazená-las para uso. Não existe a necessidade de execução deste método novamente. Caso essas credenciais expirem, elas poderão ser atualizadas com a chamada abaixo:


```
#!java
// Neste objeto colocamos os dados da aplicação solicitante
// Estes dados podem ser obtidos durante o cadastro da aplicação no site do 
// PagSeguro. Para maiores informações consulte a classe PSApp no Apêndice A
// deste documento
PSApp app = new PSApp(“appId”,”appKey”, new String[] { “perm1”, “perm2” });

// Passamos os dados da aplicação solicitante e a public key que pode ser 
// obitida com as credenciais antigas (oldCredent)
// SINCRONA (bloqueia  a thread e não pode ser chamado a partir da UIThread)
OAuthCredentials cred = PSLibrary.authorize(app, oldCredent.getPublicKey());

// * OU *

// ASSINCRONA
PSLibrary.authorize(app, oldCredent.getPublicKey(), new PSCallback<OAuthCredentials>(){ /*...*/ });

```
* **

## **Pareamento** ##

O pareamento pode ser subdividido em 2 etapas: Busca dos leitores disponíveis e Pareamento no sistema operacional Android.


### Versão sincrona ###

```
#!java
try {
// Este objeto é utilizado para o sincronismo e instalação do leitor
//no parametro do metodo scanToBond pode ser passado o id do layout (.xml)
           //para criação telas customizadas
            ReaderAddress readerAddress = PSLibrary.scanToBond(MainActivity.this.findViewById(R.id.psLib));
	
        } catch (BondCancelledException e) {
           	// exception lançada quando processo de pareamento for cancelado
        } catch (BondFailedException e) {
           	// exception lançada quando houver erro durante o processo
        }

```
* ** BondCancelledException:** Retorna uma erro de cancelamento durante o processo de pareamento.

* **BondFailedException:** Informa erro durante o processo de pareamento.


### Versão Assíncrona ###

```
#!java
PSLibrary.scanDevices(new PSLibrary.PSCallback<List<BluetoothDevice>>() {
        @Override
        public void failure(Throwable throwable) {
            if (throwable instanceof BondCancelledException) {
                showMessage("Cancelamento durante o processo de pareamento.");
            } else if (throwable instanceof BondFailedException) {
                showMessage("Erro durante o processo de pareamento.");
            }
        }
    
        @Override
        public void success(List<BluetoothDevice> result) {
            super.success(result);
    
            if (result == null || result.isEmpty()) {
                // verifica uma possível falha
                failure(null);
            } else {

                // Só encontrou um aparelho nas proximidades
                if (result.size() == 1) {
                     // Inicia o processo de Pareamento (normalmente aninhado ao fluxo de busca de dispositivos)
                     bondDevice(result);

                } else if (result.size() > 1) {
                     // ESCOLHER QUAL DEVICE UTILIZARÁ
                }
    
            }
        }
    });
```

* **

## **Instalação do leitor** ##
Após realizar o pareamento do leitor é necessário realizar sua configuração para que este receba a permissão para realizar transações. Durante este processo é necessário que o leitor esteja ligado e que o dispositivo do usuário esteja com uma conexão ativa com a internet. Para realizar a instalação execute o código abaixo:

### Versão Síncrona ###

```
#!java
// Criando objeto ReaderAddress para informar qual leitor será instalado
// Para maiores informações consulte a classe ReaderAddress no Apêndice A
ReaderAddress readerAddress = ReaderAddress.from(“00:04:91:7B:D6:CD”);

// outra forma de instalar o leitor é utilizando o readerAddress obtido pelo metodo scanToBond()

ReaderAddress readerAddress = PSLibrary.scanToBond(MainActivity.this.findViewById(R.id.psLib));

// Por ser uma operação que causa o bloqueio da Thread em execução esta
// operação deve ser executada em uma Thread separada
new AsyncTask<Void,Void,Void>() {
	@Override
       protected Void doInBackground(Void... voids) {
		// Solicitando a instalação informando as credenciais e o Mac
		// Address do leitor a ser instalado
		try {
			PSLibrary.installReader(credentials, readerAddress);
		} catch (Exception ex) {
			// Tratar os erros ocorridos
		}
	}
}.execute();

```
É importante ressaltar que por ser uma rotina de longa duração ela deve ser executada dentro de uma AsyncTask ou outra estrutura que evite o bloqueio da Thread principal do aplicativo, permitindo ao usuário da aplicação que continue a interagir com a mesma durante a execução. 


Abaixo a versão assíncrona deste serviço.

### Versão Assíncrona ###
```
#!java
// Criando objeto ReaderAddress para informar qual leitor será instalado
// Para maiores informações consulte a classe ReaderAddress no Apêndice A
ReaderAddress readerAddress = ReaderAddress.from(“00:04:91:7B:D6:CD”);

// Esta versão do serviço não causa block na Thread, portanto pode ser executada
// de qualquer thread

    PSLibrary.installReader(getCredentials(), ReaderAddress.from(readerAddress), new PSLibrary.PSCallback<Void>() {
        @Override
        public void failure(Throwable throwable) {
            super.failure(throwable);
            // VERIFICAR LISTA DE EXCECOES POSSIVEIS E AS POSSIVEIS CAUSAS
        }

        @Override
        public void success(Void result) {
            super.success(result);
            // TRATAR SUCESSO
        }
    });
```

Abaixo temos uma lista dos erros e respectivas explicações:

* **ReaderException:** Ocorre caso haja um erro de comunicação com o leitor ou durante o processamento da transação.

* **CallAbortedException**: Informa que a chamada foi cancelada.

* **InstallationException:** Informa um erro ocorrido durante o registro do leitor no aparelho.

* **IllegalStateException:** Indica que a biblioteca não foi configurada corretamente para uso antes da chamada.

* **BackendException:** Informa um erro durante a comunicação com o servidor. Este erro possuí diversas ramificações que podem ser consultadas no Apêndice B.

* **ReaderNotPairedException:** Informa um erro de dispositivo não pareado.

* **InvalidCredentialsException:** Informa um erro de crendenciais inválidas.

* **

## **Pagamento** ##
Uma vez instalado o leitor, podemos utilizar a biblioteca para realizar pagamentos. As informações referentes ao valor e forma de pagamento são informadas na chamada do método **pay** através da classe **Invoice** e, mais especificamente, sua subclasse **TEFInvoice**. Para especificar o tipo de cartão utilizado no pagamento, temos as classes **CreditPaymentCard** e **DebitPaymentCard**. Para informar as condições de pagamento utilizamos as classes **CashPayConditions** (à vista) e **InstallmentPayConditions** (parcelado). O código abaixo demonstra como estas classes são utilizadas para definir um pagamento a ser processado.

### Versão síncrona ###
```
#!java
// Definindo o tipo de cartão usado no pagamento: Crédito
PaymentCard card = new CreditPaymentCard();
// ou débito
PaymentCard card = new DebitPaymentCard();

// Especificando condição de pagamento: à vista

PaymentConditions conditions = new CashPayConditions( card );
// ou parcelado em 3 vezes
PaymentConditions conditions = InstallmentPayConditions
	.taxPaidBySeller( 3, card );

// Montando a solicitação no valor de R$5.29 para leitor de Chip e Senha

final Invoice invoice = new TEFInvoice( new BigDecimal(“5.29”), conditions );

// Recupera a view que será utilizada para realizar o pagamento (Vide documentação)
final View paymentView = buildPaymentView();

// Por ser uma operação que causa o bloqueio da Thread em execução esta
// operação deve ser executada em uma Thread separada
new AsyncTask<Void,Void,Void>() {
	@Override
        protected Void doInBackground(Void... voids) {
	    // Efetuando o pagamento solicitado
	    try {
		Transaction transaction = PSLibrary.pay( credentials, invoice, paymentView 
		);
	} catch (Exception ex) {
		// Tratar os erros ocorridos
	}
    }
}.execute();

```
Assim como no processo de instalação do leitor, temos alguns itens importantes para ressaltar neste código. Inicialmente devemos ter as mesmas preocupações alertadas anteriormente: por esta também ser uma rotina de longa duração ela deve ser executada dentro de uma estrutura que evite o bloqueio da Thread principal do aplicativo. 


### Versão assíncrona ###
```
#!java
// Definindo o tipo de cartão usado no pagamento: Crédito
PaymentCard card = new CreditPaymentCard();
// ou débito
PaymentCard card = new DebitPaymentCard();

// Especificando condição de pagamento: à vista

PaymentConditions conditions = new CashPayConditions( card );
// ou parcelado em 3 vezes
PaymentConditions conditions = InstallmentPayConditions
	.taxPaidBySeller( 3, card );

// Montando a solicitação no valor de R$5.29 para leitor de Chip e Senha

final Invoice invoice = new TEFInvoice( new BigDecimal(“5.29”), conditions );

// Recupera a view que será utilizada para realizar o pagamento (Vide documentação)
final View paymentView = buildPaymentView();

// Esta é uma versão que não bloqueia a thread, portanto pode ser chamada da UIThread
	    // Efetuando o pagamento solicitado
    PSLibrary.pay(credentials, invoice, paymentView, new PSLibrary.PSCallback<Transaction>() {
        @Override
        public void failure(Throwable throwable) {
            // TRATAR ERROS
        }

        @Override
        public void success(Transaction result) {
             // TRATAR SUCESSO
        }
    });

```


Também devemos tratar os erros que podem ocorrer durante a execução desta chamada. Abaixo temos uma lista dos erros e respectivas explicações:
* **ReaderException:** Ocorre caso haja um erro de comunicação com o leitor ou durante o processamento da transação.

* **CallAbortedException**: Informa que a chamada foi cancelada.

* **InstallationException:** Informa um erro ocorrido durante o registro do leitor no aparelho.

* **IllegalStateException:** Indica que a biblioteca não foi configurada corretamente para uso antes da chamada.

* **BackendException:** Informa um erro durante a comunicação com o servidor. Este erro possuí diversas ramificações que podem ser consultadas no Apêndice B.

* **ReaderNotPairedException:** Informa um erro de dispositivo não pareado.

* **StorageException:**Informa que houve um erro ao tentar ler as configurações salvas no dispositivo.


Além destes pontos, é preciso ressaltar que o método **pay** retorna um objeto do tipo **Transaction**. Este objeto na verdade será de um subtipo da classe **Transaction**, **TEFTransaction**. Esta classificação dos objetos permite ter maior controle sobre a forma com que a transação foi processada além de facilitar a implementação futura de novos mecanismos para o processamento de pagamentos. Esta “tipagem” também é importante pois será necessária no processo de estorno da transação caso este ocorra.

Um último e importante aspecto deste processo de pagamento é a exibição de elementos chave na tela durante sua execução. Visando facilitar a implementação por parte dos desenvolvedores e evitando impedir que forneçam uma experiência agradável em termos de layout para seus usuários, a biblioteca fornece meios que possibilitam a construção de telas que atendam aos requisitos de cada produto cuidando de todas as interações que podem ocorrer durante este processo. Para que isso seja possível o usuário deve implementar seu layout utilizando os ID's pré-definidos pela biblioteca em seu código. São definidos os ID's para os seguintes elementos:


```
#!xml

• @id/text_box_message – Texto informativo sobre a operação atual durante o processo de pagamento.
• @id/text_box_options – Opções disponibilizadas ao usuário para que o mesmo realize uma escolha entre elas.
• @id/edit_box_response – Caixa de texto para entrada da escolha do usuário.
• @id/button_continue – Botão para confirmar a escolha do usuário.
• @id/image_processing – Imagem responsável por indicar que a transação está sendo processada
• @id/button_cancel – Botão para cancelar a transação atual.
```
## Abaixo temos uma implementação sugerida para o layout desta tela: ##


```
#!xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"                
	        android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:padding="16dp"
                android:keepScreenOn="true"
                android:orientation="vertical">

    <TextView
        android:id="@id/text_box_message"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:gravity="center"
        android:layout_alignParentTop="true"
        android:typeface="monospace"/>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:gravity="center"
        android:layout_centerInParent="true"
        android:orientation="vertical">

        <TextView
            android:id="@id/text_box_options"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_margin="10dp"
            android:gravity="center"
            android:visibility="gone"
            android:typeface="monospace"/>

        <EditText
            android:id="@id/edit_box_response"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:visibility="gone"
            android:inputType="text"/>

        <Button
            android:id="@id/button_continue"
            android:layout_width="match_parent"
            android:layout_height="48dp"
            android:text="Continuar"
            android:visibility="gone"/>
    </LinearLayout>

    <ImageView
        android:id="@id/image_processing"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_centerInParent="true"
        android:padding="12dp"
        android:scaleType="center"
        android:visibility="gone"
        android:src="@drawable/reader_loading"/>
    <Button
        android:id="@id/button_cancel"
        android:layout_width="match_parent"
        android:layout_height="48dp"
        android:layout_alignParentBottom="true"
        android:text="Cancelar"
        android:visibility="gone"/>

</RelativeLayout>

```

Note que os ID's utilizados já foram pré-definidos. Além disso é importante seguir os mesmos tipos utilizados para cada controle, permitindo que durante o processamento os objetos possam ser encontrados e configurados corretamente pela biblioteca. É imprescindível que todos os elementos estejam presentes na view passada à chamada do método **pay** para que ele funcione corretamente.

Ao longo do processamento todos os elementos serão controlados pela biblioteca e poderão sumir ou reaparecer na tela ao decorrer do processamento da transação. É importante que este comportamento não seja influenciado de forma alguma pelo desenvolvedor, pois podem ocorrer cenários onde a atuação do operador do aplicativo seja requisitada. Estes cenários, apesar de rara ocorrência, são auto explicativos permitindo que o usuário facilmente forneça os dados solicitados que, em grande parte, serão informações relacionadas ao cartão do comprador.

* **

## **Estorno** ##
Dada uma transação de pagamento processada com o leitor de chip e senha, esta poderá ser estornada no mesmo dia em que foi realizada (até as 23:59h do mesmo dia), utilizando o mesmo leitor que a processou. Para realizar este procedimento utilizamos o código abaixo:

### Versão síncrona ###
```
#!java
// Recupera a view que será utilizada para realizar o pagamento (Vide documentação)
View refundView = buildPaymentView();

// Por ser uma operação que causa o bloqueio da Thread em execução esta
// operação deve ser executada em uma Thread separada
new AsyncTask<Void,Void,Void>() {
	@Override
       protected Void doInBackground(Void... voids) {
	// Efetuando o estorno da transação
	try {
		transaction = PSLibrary.refund( Credentials, transaction, refundView 
		)
                  ;
	} catch (Exception ex) {
		// Tratar os erros ocorridos
	}
     }
}.execute();

```
De forma análoga às outras operações já apresentadas, devemos nos preocupar com o fato de esta também ser uma rotina de longa duração, devendo ser executada dentro de uma estrutura que evite o bloqueio da Thread principal do aplicativo, além de tratar os erros que podem ocorrer durante sua execução. 


### Versão síncrona ###
```
#!java
// Recupera a view que será utilizada para realizar o pagamento (Vide documentação)
View refundView = buildPaymentView();

// Esta é uma chamada que não bloqueia a thread, portanto pode ser chamada de qualquer thread
PSLibrary.refund(getCredentials(), getLastTransaction(), AsynchronousActivity.this.findViewById(R.id.psLib), new PSLibrary.PSCallback<Transaction>() {
    @Override
    public void failure(Throwable throwable) {
        // TRATAR ERROS
    }

    @Override
    public void success(Transaction result) {
        // TRATAR SUCESSO
    }
});
```
Abaixo temos uma lista dos erros e respectivas explicações:

* **ReaderException:** Ocorre caso haja um erro de comunicação com o leitor ou durante o processamento da transação.

* **CallAbortedException**: Informa que a chamada foi cancelada.

* **InstallationException:** Informa um erro ocorrido durante o registro do leitor no aparelho.

* **IllegalStateException:** Indica que a biblioteca não foi configurada corretamente para uso antes da chamada.

* **BackendException:** Informa um erro durante a comunicação com o servidor. Este erro possuí diversas ramificações que podem ser consultadas no Apêndice B.

* **ReaderNotPairedException:** Informa um erro de dispositivo não pareado.

* **InvalidCredentialsException:** nforma um erro de crendenciais inválidas.

* **StorageException:**Informa que houve um erro ao tentar ler as configurações salvas no dispositivo.

* **

**UOL - O melhor conteúdo**

© 1996-2015 O melhor conteúdo. Todos os direitos reservados.
UNIVERSO ONLINE S/A - CNPJ/MF 01.109.184/0001-95 - Av. Brigadeiro Faria Lima, 1.384, São Paulo - SP - CEP 01452-002 
* **