package br.com.uol.ps.vendedorexemplo;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.http.auth.InvalidCredentialsException;

import java.math.BigDecimal;

import br.com.uol.pslibs.bluelib.BlueFinder;
import br.com.uol.pslibs.bluelib.BlueUtils;
import br.com.uol.pslibs.payment.CashPayConditions;
import br.com.uol.pslibs.payment.Configuration;
import br.com.uol.pslibs.payment.Credentials;
import br.com.uol.pslibs.payment.DebitPaymentCard;
import br.com.uol.pslibs.payment.OAuthCredentials;
import br.com.uol.pslibs.payment.PSApp;
import br.com.uol.pslibs.payment.PSLibrary;
import br.com.uol.pslibs.payment.Transaction;
import br.com.uol.pslibs.payment.business.CardPresentInvoice;
import br.com.uol.pslibs.payment.business.Invoice;
import br.com.uol.pslibs.payment.business.ReaderAddress;
import br.com.uol.pslibs.payment.exception.BackendException;
import br.com.uol.pslibs.payment.exception.CallAbortedException;
import br.com.uol.pslibs.payment.exception.InstallationException;
import br.com.uol.pslibs.payment.exception.ReaderException;
import br.com.uol.pslibs.payment.exception.ReaderNotInstalledException;
import br.com.uol.pslibs.payment.exception.ReaderNotPairedException;
import br.com.uol.pslibs.payment.exception.StorageException;
import br.com.uol.pslibs.payment.utils.StringUtils;


public class SynchronousActivity extends FragmentActivity {

    public static final String APP_ID = "<SEU APP ID>";   // TODO PREENCHER COM SEU APP ID
    public static final String APP_KEY = "<SEU APP KEY>"; // TODO PREENCHER COM SEU APP KEY

    private String mReaderMacAddress;
    private OAuthCredentials mCredentials;

    private Button mAuthButton;
    private Button mSyncButton;
    private Button mInstallButton;
    private Button mPayButton;
    private Button mRefundButton;
    private Button mAsyncButton;
    private Transaction mTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("LIFECYCLE", "onCreate");
        Configuration.Builder builder = new Configuration.Builder(this);
        builder.setLogEnabled(true);
        PSLibrary.initialize(builder.create());


        bindButtons();

        mAuthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        auth();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        updateButtons();
                    }
                }.execute();
            }
        });

        mSyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        sync();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        updateButtons();
                    }
                }.execute();
            }
        });

        mInstallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        install();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        updateButtons();
                    }
                }.execute();
            }
        });

        mPayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SynchronousActivity.this.findViewById(R.id.psLib).setVisibility(View.VISIBLE);
                SynchronousActivity.this.findViewById(R.id.actions).setVisibility(View.GONE);

                new AsyncTask<Void, Void, Transaction>() {

                    @Override
                    protected Transaction doInBackground(Void... params) {
                        return pay();
                    }

                    @Override
                    protected void onPostExecute(Transaction transaction) {
                        super.onPostExecute(transaction);

                        mTransaction = transaction;

                        SynchronousActivity.this.findViewById(R.id.psLib).setVisibility(View.GONE);
                        SynchronousActivity.this.findViewById(R.id.actions).setVisibility(View.VISIBLE);
                    }

                }.execute();
            }
        });

        mRefundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SynchronousActivity.this.findViewById(R.id.psLib).setVisibility(View.VISIBLE);
                SynchronousActivity.this.findViewById(R.id.actions).setVisibility(View.GONE);

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        refund();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        SynchronousActivity.this.findViewById(R.id.psLib).setVisibility(View.GONE);
                        SynchronousActivity.this.findViewById(R.id.actions).setVisibility(View.VISIBLE);
                    }
                }.execute();
            }
        });

        mAsyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AsynchronousActivity.class));
            }
        });
    }

    private void bindButtons() {
        mAuthButton = (Button) findViewById(R.id.auth);
        mSyncButton = (Button) findViewById(R.id.sync);
        mInstallButton = (Button) findViewById(R.id.install);
        mPayButton = (Button) findViewById(R.id.pay);
        mRefundButton = (Button) findViewById(R.id.refund);
        mAsyncButton = (Button) findViewById(R.id.async);
        updateButtons();

    }

    private BluetoothDevice deviceFound;

    private void sync() {

        BlueFinder finder = new BlueFinder(this);
        deviceFound = null;
        finder.setListener(new BlueFinder.BlueFinderListener() {
            @Override
            public void onFind(BluetoothDevice bluetoothDevice, short i) {
                if (bluetoothDevice.getName().startsWith("PAX")) {
                    Log.d("PAREADO", "FOUND THE DEVICE!!!!!!");
                    deviceFound = bluetoothDevice;
                }

                Log.d("PAREADO", "FIND device:" + bluetoothDevice.getName() + " address:" + bluetoothDevice.getAddress());
            }

            @Override
            public void onUpdate(BluetoothDevice bluetoothDevice, short i) {
                Log.d("PAREADO", "UPDATE device:" + bluetoothDevice.getName() + " address:" + bluetoothDevice.getAddress());
            }

            @Override
            public void onError() {
                showMessage("Error Finding Bluetooth devices");
            }

            @Override
            public void onFinish() {
                if (deviceFound != null) {
                    mReaderMacAddress = deviceFound.getAddress();

                    SharedPreferences.Editor edit = getSharedPreferences("PSLIB", MODE_MULTI_PROCESS).edit();
                    edit.putString("READER", mReaderMacAddress);
                    edit.commit();
                    showMessage("Device: " + mReaderMacAddress);
                }
                updateButtons();
            }

            @Override
            public void onStart() {

            }
        });

        BlueUtils.enableLogs();

        finder.listBonded();
    }

    private void install() {
        try {

            Log.d("READER", "INSTALL: " + mReaderMacAddress);
            PSLibrary.installReader(getCredentials(), ReaderAddress.from(mReaderMacAddress));

            SharedPreferences.Editor prefs = getSharedPreferences("PSLIB", MODE_PRIVATE).edit();

            prefs.putBoolean("READER_INSTALL", true);

            prefs.commit();

        } catch (ReaderException e) {
            showMessage("ReaderException");
        } catch (CallAbortedException e) {
            showMessage("CallAbortedException");
        } catch (InstallationException e) {
            showMessage("InstallationException");
        } catch (ReaderNotPairedException e) {
            showMessage("ReaderNotPairedException");
        } catch (InvalidCredentialsException e) {
            showMessage("InvalidCredentialsException");
        } catch (BackendException e) {
            showMessage("BackendException");
        }
    }

    private void showMessage(final String message) {
        new Handler(this.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SynchronousActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });

    }

    private Transaction pay() {

        Invoice invoice = new CardPresentInvoice(
                new BigDecimal("1.01"),
                new CashPayConditions(new DebitPaymentCard()));

//                Invoice invoice = new TEFInvoice(
//                        new BigDecimal("1.01"),
//                        new CashPayConditions(new CreditPaymentCard()));

//                Invoice invoice = new TEFInvoice(
//                        new BigDecimal("1.01"),
//                        InstallmentPayConditions.taxPaidBySeller(3, new CreditPaymentCard()));

        try {

            return PSLibrary.pay(getCredentials(), invoice, SynchronousActivity.this.findViewById(R.id.psLib));

        } catch (CallAbortedException e) {
            showMessage("CallAbortedException");
        } catch (ReaderException e) {
            showMessage("ReaderException");
        } catch (InstallationException e) {
            showMessage("InstallationException");
        } catch (ReaderNotInstalledException e) {
            showMessage("ReaderNotInstalledException");
        } catch (StorageException e) {
            showMessage("StorageException");
        } catch (InvalidCredentialsException e) {
            showMessage("InvalidCredentialsException");
        } catch (BackendException e) {
            showMessage("BackendException");
        }

        return null;
    }


    private void refund() {

        try {
            PSLibrary.refund(getCredentials(), getLastTransaction(), SynchronousActivity.this.findViewById(R.id.psLib));
        } catch (CallAbortedException e) {
            showMessage("CallAbortedException");
        } catch (ReaderException e) {
            showMessage("ReaderException");
        } catch (InstallationException e) {
            showMessage("InstallationException");
        } catch (ReaderNotInstalledException e) {
            showMessage("ReaderNotInstalledException");
        } catch (StorageException e) {
            showMessage("StorageException");
        } catch (InvalidCredentialsException e) {
            showMessage("InvalidCredentialsException");
        } catch (BackendException e) {
            showMessage("BackendException");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("LIFECYCLE", "onStart");
        PSLibrary.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("LIFECYCLE", "onStop");
        PSLibrary.onStop();
    }

    public void auth() {
        PSApp app = new PSApp(APP_ID, APP_KEY, new String[]{"CREATE_MOBILE_CHECKOUTS"});
        try {
            mCredentials = PSLibrary.authorize(app, this);
        } catch (BackendException e) {
            showMessage("BackendException");
            Log.e("BackendException", e.getMessage(), e);
        }
        Log.d("LIFECYCLE", "Credentials: " + (mCredentials != null));

        SharedPreferences.Editor prefs = getSharedPreferences("PSLIB", MODE_PRIVATE).edit();
        prefs.putString("CREDENTIALS", new Gson().toJson(mCredentials));
        prefs.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PSLibrary.dispose();
    }


    private boolean isAuthenticated() {
        Credentials credentials = getCredentials();
        return credentials != null;
    }

    private boolean isReaderInstalled() {
        SharedPreferences prefs = getSharedPreferences("PSLIB", MODE_MULTI_PROCESS);
        return prefs.getBoolean("READER_INSTALL", false);
    }

    public OAuthCredentials getCredentials() {

        if (mCredentials == null) {
            SharedPreferences prefs = getSharedPreferences("PSLIB", MODE_MULTI_PROCESS);
            String credentialsJSON = prefs.getString("CREDENTIALS", null);

            if (StringUtils.isNotBlank(credentialsJSON)) {
                mCredentials = new Gson().fromJson(credentialsJSON, OAuthCredentials.class);
            }
        }
        return mCredentials;
    }

    private void updateButtons() {
        mAuthButton.setCompoundDrawablesWithIntrinsicBounds(isAuthenticated() ? R.drawable.ic_check : 0, 0, 0, 0);
        mSyncButton.setCompoundDrawablesWithIntrinsicBounds(isReaderSynced() ? R.drawable.ic_check : 0, 0, 0, 0);
        mInstallButton.setCompoundDrawablesWithIntrinsicBounds(isReaderInstalled() ? R.drawable.ic_check : 0, 0, 0, 0);
    }

    private boolean isReaderSynced() {
        SharedPreferences prefs = getSharedPreferences("PSLIB", MODE_MULTI_PROCESS);
        mReaderMacAddress = prefs.getString("READER", null);
        return StringUtils.isNotBlank(mReaderMacAddress);
    }

    public Transaction getLastTransaction() {
        return mTransaction;
    }

}
