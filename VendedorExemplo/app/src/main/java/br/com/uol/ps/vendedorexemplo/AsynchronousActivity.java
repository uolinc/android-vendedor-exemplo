package br.com.uol.ps.vendedorexemplo;

import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;

import java.math.BigDecimal;
import java.util.List;

import br.com.uol.pslibs.payment.CashPayConditions;
import br.com.uol.pslibs.payment.Configuration;
import br.com.uol.pslibs.payment.Credentials;
import br.com.uol.pslibs.payment.DebitPaymentCard;
import br.com.uol.pslibs.payment.OAuthCredentials;
import br.com.uol.pslibs.payment.PSApp;
import br.com.uol.pslibs.payment.PSLibrary;
import br.com.uol.pslibs.payment.Transaction;
import br.com.uol.pslibs.payment.business.CardPresentInvoice;
import br.com.uol.pslibs.payment.business.Invoice;
import br.com.uol.pslibs.payment.business.ReaderAddress;
import br.com.uol.pslibs.payment.exception.BackendException;
import br.com.uol.pslibs.payment.utils.StringUtils;


public class AsynchronousActivity extends FragmentActivity {

    public static final String APP_ID = "<SEU APP ID>";   // TODO PREENCHER COM SEU APP ID
    public static final String APP_KEY = "<SEU APP KEY>"; // TODO PREENCHER COM SEU APP KEY

    private String mReaderMacAddress;
    private OAuthCredentials mCredentials;

    private Button mAuthButton;
    private Button mSyncButton;
    private Button mInstallButton;
    private Button mPayButton;
    private Button mRefundButton;
    private Button mAsyncButton;
    private Transaction mTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_async);
        Log.d("LIFECYCLE", "onCreate");
        Configuration.Builder builder = new Configuration.Builder(this);
        builder.setLogEnabled(true);
        PSLibrary.initialize(builder.create());

        bindButtons();

        mAuthButton.setOnClickListener(clickAuth());

        mSyncButton.setOnClickListener(syncClick());

        mInstallButton.setOnClickListener(installClick());

        mPayButton.setOnClickListener(payClick());

        mRefundButton.setOnClickListener(refundClick());

        mAsyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private View.OnClickListener refundClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AsynchronousActivity.this.findViewById(R.id.psLib).setVisibility(View.VISIBLE);
                AsynchronousActivity.this.findViewById(R.id.actions).setVisibility(View.GONE);


                PSLibrary.refund(getCredentials(), getLastTransaction(), AsynchronousActivity.this.findViewById(R.id.psLib), new PSLibrary.PSCallback<Transaction>() {
                    @Override
                    public void failure(Throwable throwable) {
                        super.failure(throwable);
                        showMessage(throwable != null ? throwable.getClass().getName() : "null exception");
                    }

                    @Override
                    public void success(Transaction result) {
                        super.success(result);
                        AsynchronousActivity.this.findViewById(R.id.psLib).setVisibility(View.GONE);
                        AsynchronousActivity.this.findViewById(R.id.actions).setVisibility(View.VISIBLE);
                    }
                });
            }
        };
    }

    private View.OnClickListener installClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("READER", "INSTALL: " + mReaderMacAddress);
                PSLibrary.installReader(getCredentials(), ReaderAddress.from(mReaderMacAddress), new PSLibrary.PSCallback<Void>() {
                    @Override
                    public void failure(Throwable throwable) {
                        super.failure(throwable);
                        showMessage(throwable != null ? throwable.getClass().getName() : "null exception");
                    }

                    @Override
                    public void success(Void result) {
                        super.success(result);
                        SharedPreferences.Editor prefs = getSharedPreferences("PSLIB", MODE_PRIVATE).edit();

                        prefs.putBoolean("READER_INSTALL", true);

                        prefs.commit();

                        updateButtons();
                    }
                });
            }
        };
    }

    private View.OnClickListener syncClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PSLibrary.scanDevices(new PSLibrary.PSCallback<List<BluetoothDevice>>() {
                    @Override
                    public void failure(Throwable throwable) {
                        showMessage("No devices found.");
                    }

                    @Override
                    public void success(List<BluetoothDevice> result) {
                        super.success(result);

                        if (result == null || result.isEmpty()) {
                            failure(null);
                        } else {

                            bondDevice(result);

                        }
                    }
                });

            }
        };
    }

    private View.OnClickListener payClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AsynchronousActivity.this.findViewById(R.id.psLib).setVisibility(View.VISIBLE);
                AsynchronousActivity.this.findViewById(R.id.actions).setVisibility(View.GONE);

                Invoice invoice = new CardPresentInvoice(
                        new BigDecimal("1.01"),
                        new CashPayConditions(new DebitPaymentCard()));

//                Invoice invoice = new TEFInvoice(
//                        new BigDecimal("1.01"),
//                        new CashPayConditions(new CreditPaymentCard()));

//                Invoice invoice = new TEFInvoice(
//                        new BigDecimal("1.01"),
//                        InstallmentPayConditions.taxPaidBySeller(3, new CreditPaymentCard()));


                PSLibrary.pay(getCredentials(), invoice, AsynchronousActivity.this.findViewById(R.id.psLib), new PSLibrary.PSCallback<Transaction>() {
                    @Override
                    public void failure(Throwable throwable) {
                        super.failure(throwable);
                        showMessage(throwable != null ? throwable.getClass().getName() : "null exception");
                    }

                    @Override
                    public void success(Transaction result) {
                        super.success(result);
                        mTransaction = result;

                        AsynchronousActivity.this.findViewById(R.id.psLib).setVisibility(View.GONE);
                        AsynchronousActivity.this.findViewById(R.id.actions).setVisibility(View.VISIBLE);
                    }
                });

            }
        };
    }

    private void bondDevice(List<BluetoothDevice> result) {
        PSLibrary.bondDevice(result.get(0), new PSLibrary.PSCallback<BluetoothDevice>() {
            @Override
            public void success(BluetoothDevice result) {
                mReaderMacAddress = result.getAddress();

                SharedPreferences.Editor edit = getSharedPreferences("PSLIB", MODE_MULTI_PROCESS).edit();
                edit.putString("READER", mReaderMacAddress);
                edit.commit();
                showMessage("Device: " + mReaderMacAddress);
            }

            @Override
            public void failure(Throwable throwable) {
                showMessage("Couldnt sync device.");
            }
        });
    }

    private View.OnClickListener clickAuth() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PSApp app = new PSApp(APP_ID, APP_KEY, new String[]{"CREATE_MOBILE_CHECKOUTS"});

                try {

                    PSLibrary.authorize(app, AsynchronousActivity.this, new PSLibrary.PSCallback<OAuthCredentials>() {
                        @Override
                        public void failure(Throwable throwable) {
                            showMessage("BackendException");
                        }

                        @Override
                        public void success(OAuthCredentials result) {
                            Log.d("LIFECYCLE", "Credentials: " + result);

                            SharedPreferences.Editor prefs = getSharedPreferences("PSLIB", MODE_PRIVATE).edit();
                            prefs.putString("CREDENTIALS", new Gson().toJson(result));
                            prefs.commit();

                            updateButtons();
                        }
                    });

                } catch (BackendException e) {
                    showMessage("BackendException");
                    Log.e("BackendException", e.getMessage(), e);
                }

            }
        };
    }

    private void bindButtons() {
        mAuthButton = (Button) findViewById(R.id.auth);
        mSyncButton = (Button) findViewById(R.id.sync);
        mInstallButton = (Button) findViewById(R.id.install);
        mPayButton = (Button) findViewById(R.id.pay);
        mRefundButton = (Button) findViewById(R.id.refund);
        mAsyncButton = (Button) findViewById(R.id.async);

        updateButtons();

    }



    private void showMessage(final String message) {
        Toast.makeText(AsynchronousActivity.this, message, Toast.LENGTH_LONG).show();
    }



    /* LIB Lifecycle updates */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("LIFECYCLE", "onStart");
        PSLibrary.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("LIFECYCLE", "onStop");
        PSLibrary.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PSLibrary.dispose();
    }



/*
*
* */
    private boolean isAuthenticated() {
        Credentials credentials = getCredentials();
        return credentials != null;
    }

    private boolean isReaderInstalled() {
        SharedPreferences prefs = getSharedPreferences("PSLIB", MODE_MULTI_PROCESS);
        return prefs.getBoolean("READER_INSTALL", false);
    }

    private OAuthCredentials getCredentials() {

        if (mCredentials == null) {
            SharedPreferences prefs = getSharedPreferences("PSLIB", MODE_MULTI_PROCESS);
            String credentialsJSON = prefs.getString("CREDENTIALS", null);

            if (StringUtils.isNotBlank(credentialsJSON)) {
                mCredentials = new Gson().fromJson(credentialsJSON, OAuthCredentials.class);
            }
        }
        return mCredentials;
    }

    private void updateButtons() {
        mAuthButton.setCompoundDrawablesWithIntrinsicBounds(isAuthenticated() ? R.drawable.ic_check : 0, 0, 0, 0);
        mSyncButton.setCompoundDrawablesWithIntrinsicBounds(isReaderSynced() ? R.drawable.ic_check : 0, 0, 0, 0);
        mInstallButton.setCompoundDrawablesWithIntrinsicBounds(isReaderInstalled() ? R.drawable.ic_check : 0, 0, 0, 0);
    }

    private boolean isReaderSynced() {
        SharedPreferences prefs = getSharedPreferences("PSLIB", MODE_MULTI_PROCESS);
        mReaderMacAddress = prefs.getString("READER", null);
        return StringUtils.isNotBlank(mReaderMacAddress);
    }

    private Transaction getLastTransaction() {
        return mTransaction;
    }

}
